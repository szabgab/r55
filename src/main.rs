pub mod parser;
pub mod resource;
pub mod submodules;
pub mod tasker;
pub mod writers;

use clap::{builder::EnumValueParser, Arg, ArgAction, Command};
use resource::ResourceType;

pub const R55_VERSION: &str = env!("CARGO_PKG_VERSION");

use std::process::ExitCode;

/// checks if the provided package string is a valid package name
fn is_valid_package_name(package: &str) -> bool {
    // did not want to include an entire regex library for such simple task
    enum States {
        Start,
        Letter,
        Error,
    }

    let mut state: States = States::Start;

    for c in package.chars() {
        match state {
            States::Start => {
                if c.is_ascii_lowercase() || c.is_ascii_digit() {
                    // check for the next letter
                    state = States::Letter
                } else {
                    // unexpected char
                    state = States::Error;
                    break;
                }
            }
            States::Letter => {
                if c.is_ascii_lowercase() || c.is_ascii_digit() {
                    state = States::Letter;
                } else if c == '.' {
                    // restart the sequence
                    state = States::Start;
                } else {
                    // unexpected char
                    state = States::Error;
                    break;
                }
            }
            States::Error => {
                break;
            }
        }
    }
    // for non error state only Letter state is allowed. Since Start state means an empty string or string that ends with full-stop
    if matches!(state, States::Letter) {
        return true;
    }

    false
}

#[test]
fn test_is_valid_package_name() {
    assert!(is_valid_package_name("com.example"));
    assert!(is_valid_package_name("com.example.r"));
    assert!(is_valid_package_name("com"));
    assert!(!is_valid_package_name(".com.example"));
    assert!(!is_valid_package_name(""));
    assert!(!is_valid_package_name("com..example"));
}

/// Entry point
fn main() -> ExitCode {
    // create and parse command line arguments
    let cmd = clap::Command::new("r55").version(R55_VERSION);
    let package_args = Arg::new("package")
        .short('p')
        .long("package")
        .help("Package name for generated classes")
        .required(true);

    let input_file_args = Arg::new("input")
        .required(true)
        .help("The input R.txt file to parse");

    let split_arg = Arg::new("split")
        .action(ArgAction::SetTrue)
        .long("split")
        .help("Generate additional files based on input R.txt filters")
        .long_help("r55 will parse the main input R.txt file and generate classes based on id(s) listed in filter R.txt files. This is useful for generating library specific R class files without including other external libraries resource ids. Requires --files and --packages flags as inputs.");

    let split_files = Arg::new("files")
        .help("A list of filter R.txt files to use. colon (:) separated list.")
        .short('F')
        .long("files")
        .num_args(0..)
        .value_delimiter(':')
        .requires("packages")
        .required_if_eq("split", "true");

    let split_packages = Arg::new("packages")
        .help("A list of packages corresponding to filter files. colon (:) separated list.")
        .long("packages")
        .short('P')
        .num_args(0..)
        .value_delimiter(':')
        .required_if_eq("split", "true")
        .requires("files");

    let output_file = Arg::new("output")
        .required(false)
        .help("The output path to write the generated R.java")
        .short('o')
        .long("output");

    // find sub command
    let find_subcmd = Command::new("find")
        .about("Finds the value of the provided resource id")
        .arg(&input_file_args)
        .arg(
            Arg::new("id")
                .required(true)
                .help("The resource id to obtain its value"),
        )
        .arg(
            Arg::new("type")
                .long("type")
                .short('t')
                .value_parser(EnumValueParser::<ResourceType>::new())
                .help("Filter to a specific resource type e.g. anim"),
        );

    // java sub command
    let java_subcmd = Command::new("java")
        .about("generates a R.java class from the given R.txt file.")
        .arg(&package_args)
        .arg(&input_file_args)
        .arg(&output_file.clone().required_if_eq("split", "true"))
        .args([&split_arg, &split_files, &split_packages]);

    let compile_subcmd = Command::new("compile")
        .about("Compiles R.txt file to a java class bytecode (.class) ")
        .arg(input_file_args)
        .arg(package_args)
        .arg(output_file.required(true))
        .args([&split_arg, &split_files, &split_packages]);

    let mut cmd = cmd.subcommands([java_subcmd, find_subcmd, compile_subcmd]);

    let matches = cmd.get_matches_mut();

    // Check for subcommands provided
    match matches.subcommand() {
        Some(("java", java_matches)) => {
            if java_matches.get_flag("split") {
                submodules::java_split(java_matches);
            } else {
                submodules::java(java_matches);
            }
            ExitCode::SUCCESS
        }
        Some(("find", find_matches)) => submodules::find(find_matches),
        Some(("compile", compile_matches)) => {
            if compile_matches.get_flag("split") {
                submodules::compile_split(compile_matches)
            } else {
                submodules::compile(compile_matches)
            }
        }
        _ => {
            if let Err(err) = cmd.print_long_help() {
                eprintln!("[-] {err}");
            }
            ExitCode::FAILURE
        }
    }

    // match matches {}
}
