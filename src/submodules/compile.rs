use std::{
    fs::{create_dir_all, File},
    io::BufWriter,
    iter::zip,
    path::PathBuf,
    process::ExitCode,
};

use clap::ArgMatches;

use crate::{
    is_valid_package_name,
    parser::Parser,
    resource::ResourceType,
    writers::{
        bytecode::{BytecodeWriter, ClassWriter},
        R55Writer,
    },
};

use super::group_entries;
use super::split;

pub fn compile_split(matches: &ArgMatches) -> ExitCode {
    let package: &String = matches.get_one("package").unwrap();
    let input_file: &String = matches.get_one("input").unwrap();
    let output_file: &String = matches.get_one("output").unwrap();

    let out_path = PathBuf::from(output_file);
    let input_path = PathBuf::from(input_file);

    if !input_path.exists() {
        eprintln!("[x] Input file does not exist.");
        return ExitCode::FAILURE;
    }

    if !out_path.exists() {
        eprintln!("[x] Output folder does not exist. Please provide a valid directory.");
        return ExitCode::FAILURE;
    }

    // confirm package is valid
    if !is_valid_package_name(package) {
        eprintln!("[x] Invalid java package name. Please provide a valid package name. e.g. com.example.java");
        return ExitCode::FAILURE;
    }

    // get packages and files arg
    let packages: Vec<&String> = matches
        .get_many("packages")
        .expect("Packages list is required")
        .collect();
    let files: Vec<&String> = matches
        .get_many("files")
        .expect("Files list and corresponding package list is required.")
        .collect();

    if packages.len() != files.len() {
        eprintln!("[-] Invalid number of packages and filter files. Each file should have a corresponding package name therefore equal number of files entries and package entries");
        return ExitCode::FAILURE;
    }

    // confirm package names
    let mut has_errors = false;
    let mut writers: Vec<Box<dyn R55Writer + Send>> = Vec::new();
    for (i, (file, package)) in zip(files, packages).enumerate() {
        // check for package name
        if !is_valid_package_name(package) {
            eprintln!(
                "[-] Invalid package name \"{}\" at packages argument list index {}",
                package, i
            );
            has_errors = true;
        }
        // check files
        let input_file = PathBuf::from(file);
        if !input_path.exists() {
            eprintln!(
                "[-] File not found, \"{}\" from files argument list index {}",
                file, i
            );
            has_errors = true;
        }

        let writer = Box::new(BytecodeWriter::new(input_file, &out_path, package));
        writers.push(writer);
    }
    if has_errors {
        return ExitCode::FAILURE;
    }

    let mut parser = Parser::new();
    let entries = match parser.parse_file_hash_map(&input_path) {
        Err(err) => {
            eprintln!("[x] Parser error. Line {}, {}", parser.line_number, err.msg);
            return ExitCode::FAILURE;
        }
        Ok(entries) => entries,
    };

    let main_writer = Box::new(BytecodeWriter::new(input_path, &out_path, package));

    split(main_writer, writers, entries);
    ExitCode::SUCCESS
}

pub fn compile(matches: &ArgMatches) -> ExitCode {
    let input_file: &String = matches.get_one("input").unwrap();
    let output_file: &String = matches.get_one("output").unwrap();
    let package: &String = matches.get_one("package").unwrap();

    let mut out_path = PathBuf::from(output_file);
    let input_path = PathBuf::from(input_file);

    if !input_path.exists() {
        eprintln!("[x] Input file does not exist.");
        return ExitCode::FAILURE;
    }

    if !out_path.exists() {
        eprintln!("[x] Output folder does not exist. Please provide a valid directory.");
        return ExitCode::FAILURE;
    }

    if !is_valid_package_name(package) {
        eprintln!("[x] Invalid java package name. Please provide a valid package name. e.g. com.example.java");
        return ExitCode::FAILURE;
    }

    let mut parser = Parser::new();

    let entries = match parser.parse_file_vec(&input_path) {
        Ok(entries) => entries,
        Err(err) => {
            eprintln!("[x] Failed to parse file: {}", err.msg);
            return ExitCode::FAILURE;
        }
    };

    // group the entries, each group will be its own class
    let groups = group_entries(entries);

    // prepare the directory for dumping binary files
    out_path.extend(package.split('.'));
    if let Err(err) = create_dir_all(&out_path) {
        eprintln!("[x] Failed to prepare package tree directories. {}", err);
        return ExitCode::FAILURE;
    }

    // write inner classes
    for (res_type, entries) in &groups {
        let mut class_file = out_path.clone();
        class_file.push(format!("R${}.class", res_type));
        let file = match File::create(&class_file) {
            Ok(file) => file,
            Err(error) => {
                eprintln!(
                    "[x] Failed to create output file {}. {}",
                    class_file.to_str().unwrap_or(""),
                    error
                );
                return ExitCode::FAILURE;
            }
        };

        let mut writer = BufWriter::new(file);
        let mut class_writer = ClassWriter::new(&mut writer, package, *res_type, entries);

        match class_writer.write_inner_class() {
            Ok(_) => {}
            Err(error) => {
                eprintln!("[x] Failed to write class file. {}", error);
            }
        }
    }

    // write outer class, R.class
    let mut class_file = out_path.clone();
    class_file.push("R.class");

    let file = match File::create(&class_file) {
        Ok(file) => file,
        Err(error) => {
            eprintln!(
                "[x] Failed to create output file {}. {}",
                class_file.to_str().unwrap_or(""),
                error
            );
            return ExitCode::FAILURE;
        }
    };
    let mut writer = BufWriter::new(file);
    let entries_empty = Vec::new();
    let mut class_writer = ClassWriter::new(
        &mut writer,
        package,
        crate::resource::ResourceType::UnknownRes,
        &entries_empty,
    );

    let members: Vec<ResourceType> = groups.iter().map(|e| e.0).collect();
    match class_writer.write_outer_class(&members) {
        Ok(_) => {}
        Err(error) => {
            eprintln!("[x] Failed to write class file. {}", error);
        }
    }

    ExitCode::SUCCESS
}

// And with this now, I have ascended. I'm no longer human since I understand java bytecode
