use std::{
    num::NonZeroUsize,
    sync::{
        mpsc::{self, Receiver, Sender},
        Arc, Mutex,
    },
    thread::{self, JoinHandle},
};

/// Maintains a pool of reusable threads that operate on tasks
pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: Sender<Message>,
}

pub struct Worker {
    thread: JoinHandle<()>,
    pub id: usize,
}

type Job = Box<dyn FnOnce() + Send + 'static>;
/// Used to specify the type of message that was sent.
pub enum Message {
    /// A job
    Task(Job),
}

impl ThreadPool {
    /// Creates a new thread pool with specified number of worker threads
    pub fn new(count: NonZeroUsize) -> ThreadPool {
        let mut workers = Vec::with_capacity(count.get());
        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        for i in 0..count.get() {
            workers.push(Worker::new(i, Arc::clone(&receiver)))
        }
        ThreadPool { workers, sender }
    }
    /// Add the closure to a task pool
    pub fn execute(&self, task: Message) -> Result<(), mpsc::SendError<Message>> {
        self.sender.send(task)
    }
    /// Sends an end signal to all workers and waits on all pending tasks
    pub fn end(self) {
        // end sending of messages
        drop(self.sender);
        // wait on other threads that may be working
        for worker in self.workers {
            // i dont see why we should handle the error but just propagate it instead
            worker.thread.join().unwrap();
        }
    }
}

impl Worker {
    pub fn new(id: usize, receiver: Arc<Mutex<Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || {
            loop {
                let message = receiver
                    .lock()
                    .unwrap_or_else(|_| panic!("Job #{} failed to obtain lock.", id))
                    .recv();

                if message.is_err() {
                    // The sender disconnected so kill the worker
                    break;
                }

                let message = message.unwrap();

                match message {
                    Message::Task(job) => {
                        job();
                    }
                }
            }
        });
        Worker { thread, id }
    }
}
