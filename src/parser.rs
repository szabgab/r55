use std::collections::HashMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::num::ParseIntError;
use std::path::Path;

use crate::resource::ResourceArrayEntry;
use crate::resource::ResourceEntry;
use crate::resource::ResourceType;

#[derive(Debug)]
/// Information about where the parser leftoff during the last iteration.
enum ParserState {
    /// Parser initial state, this tells the parser that it is starting a new entry
    Start,
    /// Try to parse a single line entry e.g. int anim abc_fade_in 0x7f010000
    ReadEntry(ReadEntryState),
    /// Try to parse an array entry e.g. int[] styleable ActionMode { 0x7f03003a, 0x7f030041, 0x7f0300ab, 0x7f03017b, 0x7f0302ea, 0x7f030350 }
    ReadArrayEntry(ReadArrayEntryState),
    /// Try to map array entries from the parent entry e.g. int styleable ActionMode_background 0
    MapArrayEntries(usize, ReadArrayEntryIdState),
    /// Commit the temporary current_entry. The parser is done and confident with its work so far
    End,
}

#[derive(Debug)]
/// Individual states of the single line entry
/// for `int anim abc_fade_in 0x7f010000`
/// anim -> type
/// abc_fade_in -> id
/// 0x7f010000 -> value
/// we don't check for int since that's what brought us to this state anyway
enum ReadEntryState {
    /// Parse the resource type e.g. anim to `ResourceType::anim`
    Type,
    /// Parse the resource id e.g. abc_fade_in
    Id,
    /// Parse hex value to a u32 field
    Value,
}

#[derive(Debug)]
/// Individual states of a parent array entry e.g.
/// for `int[] styleable ActionMode { 0x7f03003a, 0x7f030041, 0x7f0300ab, 0x7f03017b, 0x7f0302ea, 0x7f030350 }`
/// styleable -> type
/// ActionMode -> id
// { -> opening brackets
/// 0x7f03003a, ',' -> values until '}'
/// we don't check for int[] since that's what brought us to this state anyway
enum ReadArrayEntryState {
    /// Parse the resource type e.g. styleable to `ResourceType::styleable`
    Type,
    /// Parse the resource id e.g. ActionMode
    Id,
    /// Check for opening brackets "{"
    OpeningBrackets,
    /// Check for hexadecimal values to parse
    Values,
}

#[derive(Debug)]
/// Used for mapping Array Entries values with this array entry id e.g.
/// for `int styleable ActionMode_background 0`
/// int -> value type
/// styleable -> type
/// ActionMode_background -> id
/// 0 -> index
enum ReadArrayEntryIdState {
    /// Used by the parser to confirm that the value type is int
    ValueType,
    /// Parse the resource type e.g. styleable to `ResourceType::styleable` and make sure it marches parent resource type
    Type,
    /// Parse the resource id e.g. ActionMode_background
    Id,
    /// Parse the index value and ensure its within range of parent index
    Index,
}

/// The parser error that is returned if parser related errors occur
pub struct ParserError {
    /// type of parser error
    pub kind: ParserErrorKind,
    /// The detailed error message
    pub msg: String,
    /// Stores inner `io::Error` if one occurred
    pub io_error: Option<std::io::Error>,
}

/// Parser error types
pub enum ParserErrorKind {
    /// The entry type was not found in valid `ResourceType`
    InvalidEntryType,
    /// The value type is expected to be int but another value was found.
    InvalidValueType,
    /// The index type is expected to be int but something else was found.
    MismatchedArrayIndexType,
    /// parent entry type does not match the child array entry type.
    MismatchedEntryType,
    /// child array entry index value cannot be parsed into a valid usize
    InvalidIndexValue,
    /// child array entry had an index that was out of range respective of parent entry array
    IndexOutOfRange,
    /// An underlying IO Error occurred
    IOError,
}

impl ParserError {
    pub fn new(kind: ParserErrorKind, msg: String) -> Self {
        ParserError {
            kind,
            msg,
            io_error: None,
        }
    }
}

/// Parses an aapt2 generated R.txt
pub struct Parser {
    /// current parser state
    state: ParserState,
    /// Temporary entry that is being worked on
    current_entry: ResourceEntry,
    /// The parser current line number
    pub line_number: u32,
}

impl Parser {
    /// Creates a new parser instance with default values
    pub fn new() -> Parser {
        Parser {
            state: ParserState::Start,
            current_entry: ResourceEntry::new(),
            line_number: 0,
        }
    }
    /// Parses the provided file into a hash map
    /// The hash map keys are a combination of ResourceType and ResourceEntry.id e.g.
    /// for int xml standalone_badge_offset is xml_standalone_badge_offset.
    ///
    /// Returns Error when:
    ///  - Parser fails to parse input
    ///  - Failed to open file or other IO error
    pub fn parse_file_hash_map(
        &mut self,
        path: &Path,
    ) -> std::result::Result<HashMap<String, ResourceEntry>, ParserError> {
        let file = File::open(path).map_err(|err| ParserError {
            kind: ParserErrorKind::IOError,
            msg: format!("Failed opening file: {}", path.to_str().unwrap_or("")),
            io_error: Some(err),
        })?;

        let mut reader = BufReader::new(file);
        let mut entries = HashMap::new();

        let mut buf = String::new();
        while let Ok(read_bytes) = reader.read_line(&mut buf) {
            if read_bytes == 0 {
                break;
            }
            self.line_number += 1;

            self.parse_line(buf)?;

            if let ParserState::End = self.state {
                let entry = self.current_entry.clone();
                entries.insert(format!("{}_{}", entry.res_type, entry.id), entry);
                self.current_entry = ResourceEntry::new();

                self.state = ParserState::Start;
            }
            buf = String::new();
        }
        Ok(entries)
    }

    /// Parses the provided file path into a vector of entries
    /// Returns error when:
    ///  - Parser failed to parse input
    ///  - Failed to open file or other IO error
    pub fn parse_file_vec(
        &mut self,
        path: &Path,
    ) -> std::result::Result<Vec<ResourceEntry>, ParserError> {
        let file = File::open(path).map_err(|err| ParserError {
            kind: ParserErrorKind::IOError,
            msg: format!("Failed opening file: {}", path.to_str().unwrap_or("")),
            io_error: Some(err),
        })?;

        let mut reader = BufReader::new(file);
        let mut entries: Vec<ResourceEntry> = Vec::new();

        let mut buf = String::new();
        while let Ok(read_bytes) = reader.read_line(&mut buf) {
            if read_bytes == 0 {
                break;
            }
            self.line_number += 1;

            self.parse_line(buf)?;

            if let ParserState::End = self.state {
                let entry = self.current_entry.clone();
                entries.push(entry);
                self.current_entry = ResourceEntry::new();

                self.state = ParserState::Start;
            }
            buf = String::new();
        }
        Ok(entries)
    }
    /// Parses a single line of input
    /// This is not intended for external use since it does not return the entry
    /// but instead updates the parser state.
    fn parse_line(&mut self, line: String) -> std::result::Result<(), ParserError> {
        let line = line.trim();
        let tokens: Vec<&str> = line.split_whitespace().collect();
        for i in 0..tokens.len() {
            let token = *tokens
                .get(i)
                .expect("Unreachable state reached. This is bad"); // should not fail indexing

            match &self.state {
                // check the starting token
                ParserState::Start => {
                    if token == "int" {
                        self.state = ParserState::ReadEntry(ReadEntryState::Type);
                    } else if token == "int[]" {
                        self.current_entry.is_array = true;
                        self.state = ParserState::ReadArrayEntry(ReadArrayEntryState::Type);
                    } else {
                        return Err(ParserError::new(
                            ParserErrorKind::InvalidEntryType,
                            format!(
                                "Invalid entry type, only int and int[] are currently supported: found {}"
                            , token),
                        ));
                    }
                }
                // Read single entry
                ParserState::ReadEntry(entry_state) => match entry_state {
                    ReadEntryState::Type => {
                        let res_type = ResourceType::from(token);
                        self.current_entry.res_type = res_type;
                        self.state = ParserState::ReadEntry(ReadEntryState::Id);
                    }
                    ReadEntryState::Id => {
                        self.current_entry.id = token.to_string();
                        self.state = ParserState::ReadEntry(ReadEntryState::Value);
                    }
                    ReadEntryState::Value => {
                        self.current_entry.value = if let Ok(val) = Parser::hex_to_u32(token) {
                            val
                        } else {
                            return Err(ParserError::new(
                                ParserErrorKind::InvalidValueType,
                                "Expected non negative hexadecimal value of type int.".to_string(),
                            ));
                        };

                        self.state = ParserState::End;
                    }
                },
                // Read Array entries
                ParserState::ReadArrayEntry(entry_state) => match entry_state {
                    ReadArrayEntryState::Type => {
                        let res_type = ResourceType::from(token);
                        self.current_entry.res_type = res_type;
                        self.state = ParserState::ReadArrayEntry(ReadArrayEntryState::Id);
                    }
                    ReadArrayEntryState::Id => {
                        self.current_entry.id = token.to_string();
                        self.state =
                            ParserState::ReadArrayEntry(ReadArrayEntryState::OpeningBrackets);
                    }
                    ReadArrayEntryState::OpeningBrackets => {
                        if token == "{" {
                            self.state = ParserState::ReadArrayEntry(ReadArrayEntryState::Values);
                        } else if token.starts_with('{') {
                            let token = token.trim_matches(&['{', '0', 'x', ',']);
                            self.current_entry.value =
                                if let Ok(val) = u32::from_str_radix(token, 16) {
                                    val
                                } else {
                                    return Err(ParserError::new(
                                        ParserErrorKind::InvalidValueType,
                                        "Expected non negative hexadecimal value of type int."
                                            .to_string(),
                                    ));
                                };

                            self.state = ParserState::ReadArrayEntry(ReadArrayEntryState::Values);
                        } else {
                            return Err(ParserError::new(
                                ParserErrorKind::InvalidValueType,
                                format!("Expected '{{', found {}", token),
                            ));
                        }
                    }
                    ReadArrayEntryState::Values => {
                        if token == "," {
                            continue;
                        }

                        if token == "}" {
                            if self.current_entry.entries.is_empty() {
                                self.state = ParserState::End;
                                continue;
                            }
                            self.state =
                                ParserState::MapArrayEntries(0, ReadArrayEntryIdState::ValueType);
                            continue;
                        }

                        let value = if let Ok(val) = Parser::hex_to_u32(token) {
                            val
                        } else {
                            return Err(ParserError::new(
                                ParserErrorKind::InvalidValueType,
                                "Expected non negative hexadecimal value of type int.".to_string(),
                            ));
                        };
                        self.current_entry.entries.push(ResourceArrayEntry {
                            id: String::new(),
                            value,
                        });
                        self.state = ParserState::ReadArrayEntry(ReadArrayEntryState::Values)
                    }
                },
                ParserState::MapArrayEntries(count, entry_state) => match entry_state {
                    ReadArrayEntryIdState::ValueType => {
                        if token == "int" {
                            self.state =
                                ParserState::MapArrayEntries(*count, ReadArrayEntryIdState::Type);
                            continue;
                        }
                        return Err(ParserError::new(ParserErrorKind::MismatchedArrayIndexType, format!("Expected 'int' but found '{}'. The array entry index type is expected to be int.",token)));
                    }
                    ReadArrayEntryIdState::Type => {
                        let res_type = ResourceType::from(token);
                        if res_type != self.current_entry.res_type {
                            return Err(ParserError::new(
                                ParserErrorKind::MismatchedEntryType,
                                format!(
                                    "Expected array entry type of '{}' but found '{}' ",
                                    self.current_entry.res_type, token
                                ),
                            ));
                        }
                        self.state =
                            ParserState::MapArrayEntries(*count, ReadArrayEntryIdState::Id);
                    }
                    ReadArrayEntryIdState::Id => {
                        // usually i would store this id in a temporary value but use the previous index in the next state instead
                        self.state =
                            ParserState::MapArrayEntries(*count, ReadArrayEntryIdState::Index);
                    }
                    ReadArrayEntryIdState::Index => {
                        // read index value
                        let index = if let Ok(val) = token.parse::<usize>() {
                            val
                        } else {
                            return Err(ParserError::new(ParserErrorKind::InvalidIndexValue, format!("Expected unsigned integer values as array index but found '{}'", token)));
                        };
                        if index >= self.current_entry.entries.len() {
                            return Err(ParserError::new(ParserErrorKind::IndexOutOfRange, format!("Index out of range. The valid index is between 0 and {}, but found {}", self.current_entry.entries.len(), token)));
                        }

                        if let Some(entry) = self.current_entry.entries.get_mut(index) {
                            entry.id = tokens
                                .get(i - 1)
                                .expect("Unexpected error occurred.")
                                .to_string();
                            // error should not occur
                        }

                        if *count < self.current_entry.entries.len() - 1 {
                            self.state = ParserState::MapArrayEntries(
                                count + 1,
                                ReadArrayEntryIdState::ValueType,
                            );
                        } else {
                            self.state = ParserState::End;
                        }
                    }
                },
                ParserState::End => {}
            }
        }
        Ok(())
    }
    fn hex_to_u32(token: &str) -> Result<u32, ParseIntError> {
        let token = token.trim_end_matches([',', '}']);
        if token.starts_with("0x") {
            u32::from_str_radix(token.strip_prefix("0x").unwrap(), 16)
        } else {
            u32::from_str_radix(token, 16)
        }
    }
}

impl Default for Parser {
    fn default() -> Self {
        Self::new()
    }
}
