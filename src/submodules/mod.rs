pub mod compile;

use std::{
    collections::HashMap,
    fs::File,
    io::BufWriter,
    iter::zip,
    num::NonZeroUsize,
    path::PathBuf,
    process::ExitCode,
    sync::Arc,
    thread::{self},
};

use clap::{ArgMatches, ValueEnum};

use crate::{
    is_valid_package_name,
    parser::Parser,
    resource::{ResourceEntry, ResourceType},
    tasker::Message,
    tasker::ThreadPool,
    writers::{
        java::{write_java, JavaWriter},
        R55Writer,
    },
};

pub use compile::compile;
pub use compile::compile_split;

/// Groups entries by resource type
fn group_entries(entries: Vec<ResourceEntry>) -> Vec<(ResourceType, Vec<ResourceEntry>)> {
    const ARRAY_REPEAT_VALUE: Option<(ResourceType, std::vec::Vec<ResourceEntry>)> = None;
    let mut groups: [Option<(ResourceType, Vec<ResourceEntry>)>; 29] = [ARRAY_REPEAT_VALUE; 29];

    for entry in entries {
        let index: usize = usize::from(&entry.res_type);

        match groups.get_mut(index) {
            Some(group) => match group {
                Some((_, array)) => array.push(entry),
                None => groups[index] = Some((entry.res_type, vec![entry])),
            },
            None => {
                // this is bad
                unreachable!()
            }
        }
    }

    groups
        .iter()
        .filter(|e| e.is_some())
        .map(|e| e.to_owned())
        .map(|e| e.unwrap())
        .collect()
}

/// Parses the provided R.txt file and generates corresponding R.java file.
/// If output path was provided, It creates a file with a full package directory tree
/// If no output path, it writes to stdout instead
pub fn java(java_matches: &ArgMatches) {
    let package: &String = java_matches.get_one("package").unwrap();
    let input: &String = java_matches.get_one("input").unwrap();
    let output: Option<&String> = java_matches.get_one("output");

    let path = PathBuf::from(input);
    // confirm input path is present
    if !path.exists() {
        eprintln!(
            "[-] The path provided, {} does not exist. ",
            path.to_str().unwrap_or("")
        );
    }

    // confirm package is valid
    if !is_valid_package_name(package.as_str()) {
        eprintln!("[-] Invalid package name provided. Please provide a valid package name. e.g. com.example.myapp");
        return;
    }
    // check if output path is provided
    if let Some(out) = output {
        let mut outpath = PathBuf::from(out);
        if !outpath.exists() {
            eprintln!(
                "[-] The provided output path, {} does not exist.",
                outpath.to_str().unwrap_or("")
            );
            return;
        }
        package.split('.').for_each(|p| outpath.push(p));

        // create package directory tree
        if let Err(err) = std::fs::create_dir_all(&outpath) {
            eprintln!(
                "[x] Failed to create full package path {}. {err}",
                outpath.to_str().unwrap_or("")
            );
        }

        outpath.push("R.java");

        let file = match File::create(&outpath) {
            Ok(file) => file,
            Err(error) => {
                eprintln!(
                    "[x] Failed to open '{}'. {} ",
                    outpath.to_str().unwrap_or(""),
                    error
                );
                return;
            }
        };

        let mut parser = Parser::new();
        match parser.parse_file_vec(&path) {
            Err(err) => {
                println!("[x] Line {} {}", parser.line_number, err.msg);
            }
            Ok(entries) => {
                if let Err(err) =
                    write_java(&mut BufWriter::new(file), group_entries(entries), package)
                {
                    eprintln!("[x] IO error, {err}");
                }
            }
        };
    } else {
        let mut parser = Parser::new();
        match parser.parse_file_vec(&path) {
            Err(err) => {
                println!("[x] Line {} {}", parser.line_number, err.msg);
            }
            Ok(entries) => {
                if let Err(err) = write_java(
                    &mut BufWriter::new(BufWriter::new(std::io::stdout())),
                    group_entries(entries),
                    package,
                ) {
                    eprintln!("[x] IO error, {err}");
                }
            }
        };
    }
}

pub fn java_split(java_matches: &ArgMatches) {
    let package: &String = java_matches.get_one("package").unwrap();
    let input: &String = java_matches.get_one("input").unwrap();
    let output: &String = java_matches.get_one("output").unwrap();

    let path = PathBuf::from(input);
    // confirm input path is present
    if !path.exists() {
        eprintln!(
            "[-] The path provided, {} does not exist. ",
            path.to_str().unwrap_or("")
        );
    }

    // confirm package is valid
    if !is_valid_package_name(package.as_str()) {
        eprintln!("[-] Invalid package name provided. Please provide a valid package name. e.g. com.example.myapp");
        return;
    }

    let output_path = PathBuf::from(output);
    if !output_path.exists() {
        eprintln!(
            "[-] The provided output path, {} does not exist.",
            output_path.to_str().unwrap_or("")
        );
        return;
    }

    // get packages and files arg
    let packages: Vec<&String> = java_matches
        .get_many("packages")
        .expect("Packages list is required")
        .collect();
    let files: Vec<&String> = java_matches
        .get_many("files")
        .expect("Files list and corresponding package list is required.")
        .collect();

    if packages.len() != files.len() {
        eprintln!("[-] Invalid number of packages and filter files. Each file should have a corresponding package name therefore equal number of files entries and package entries");
        return;
    }

    // confirm package names
    let mut has_errors = false;
    for (i, package) in packages.iter().enumerate() {
        if !is_valid_package_name(package) {
            eprintln!(
                "[-] Invalid package name \"{}\" at packages argument list index {}",
                package, i
            );
            has_errors = true;
        }
    }
    if has_errors {
        return;
    }

    let mut has_errors = false;
    let mut paths: Vec<PathBuf> = Vec::new();
    for (i, file) in files.iter().enumerate() {
        let p = PathBuf::from(file);
        if !p.exists() {
            eprintln!(
                "[-] File not found, \"{}\" from files argument list index {}",
                file, i
            );
            has_errors = true;
        }
        paths.push(p);
    }

    if has_errors {
        return;
    }

    let mut parser = Parser::new();
    let entries = match parser.parse_file_hash_map(&path) {
        Err(err) => {
            eprintln!("[x] Parser error. Line {}, {}", parser.line_number, err.msg);
            return;
        }
        Ok(entries) => entries,
    };

    let mut writers: Vec<Box<dyn R55Writer + Send>> = Vec::new();
    for (path, package) in zip(paths, packages) {
        let java_writer = JavaWriter::new(path, &output_path, package);
        writers.push(Box::new(java_writer));
    }

    let main_writer = Box::new(JavaWriter::new(path, &output_path, package));

    split(main_writer, writers, entries);
}

pub fn split(
    main_writer: Box<dyn R55Writer + Send>,
    writers: Vec<Box<dyn R55Writer + Send>>,
    entries: HashMap<String, ResourceEntry>,
) {
    // we need to parse the input files

    let main_entries = Arc::new(entries);

    let parallelism_count =
        thread::available_parallelism().unwrap_or(NonZeroUsize::new(2).unwrap());

    let thread_pool = ThreadPool::new(parallelism_count);

    for writer in writers {
        let main_entries = Arc::clone(&main_entries);
        let task = Message::Task(Box::new(move || {
            let file = writer.get_input_file();
            let mut parser = Parser::new();
            let mut entries = match parser.parse_file_vec(file) {
                Ok(entries) => entries,
                Err(err) => {
                    eprintln!(
                        "[x] Parser error({}). Line {}, {}",
                        file.to_string_lossy(),
                        parser.line_number,
                        err.msg
                    );
                    return;
                }
            };
            for entry in entries.iter_mut() {
                let id = format!("{}_{}", entry.res_type, entry.id);
                if let Some(e) = main_entries.get(&id) {
                    entry.value = e.value;
                }
            }
            let grouped = group_entries(entries);
            if let Err(err) = writer.write(grouped) {
                eprintln!("[x] Writer Error for {}: {}", file.to_string_lossy(), err);
            }
        }));
        thread_pool.execute(task).unwrap();
    }

    // write main file
    thread_pool
        .execute(Message::Task(Box::new(move || {
            // TODO make this more efficient than just clonning everything
            let entries: Vec<ResourceEntry> = main_entries.values().cloned().collect();
            let groups = group_entries(entries);

            if let Err(err) = main_writer.write(groups) {
                eprintln!(
                    "[x] Writer Error for {}: {}",
                    main_writer.get_input_file().to_string_lossy(),
                    err
                );
            }
        })))
        .unwrap();

    // wait for tasks to conclude
    thread_pool.end();
}

/// Tries to locate a given resource id value from a R.txt
/// If found it outputs the hexadecimal value and returns 0
/// else returns error code
/// 0 - success
/// 3 - Parser error
/// 4 - IO Error
pub fn find(find_matches: &ArgMatches) -> ExitCode {
    let input_file: &String = find_matches.get_one("input").unwrap();
    let id: &String = find_matches.get_one("id").unwrap();
    let res_type: Option<&ResourceType> = find_matches.get_one("type");

    let path = PathBuf::from(input_file);
    if !path.exists() {
        eprintln!(
            "[-] The path provided, {} does not exist. ",
            path.to_str().unwrap_or("")
        );
        return ExitCode::from(4);
    }

    // parse file
    let mut parser = Parser::new();
    let entries = match parser.parse_file_hash_map(&path) {
        Err(err) => {
            eprintln!("[x] Line {} {}", parser.line_number, err.msg);
            return ExitCode::from(3);
        }
        Ok(entries) => entries,
    };

    if let Some(res_type) = res_type {
        let key = format!("{}_{}", res_type, id);
        if let Some(entry) = entries.get(&key) {
            if entry.is_array {
                print!("int[] {} {{ ", res_type);
                for i in 0..entry.entries.len() {
                    let child_entry = entry.entries.get(i).unwrap();
                    if i == entry.entries.len() - 1 {
                        println!("0x{:0>8x} }} ", child_entry.value);
                    } else {
                        print!("0x{:0>8x}, ", child_entry.value);
                    }
                }
            } else {
                println!("int {} 0x{:0>8x}", res_type, entry.value);
            }
        } else {
            return ExitCode::from(6);
        }
    } else {
        let possible_types = ResourceType::value_variants();
        let mut found = false;
        for res_type in possible_types {
            let key = format!("{}_{}", res_type, id);
            if let Some(entry) = entries.get(&key) {
                if entry.is_array {
                    print!("int[] {} {{ ", res_type);
                    for i in 0..entry.entries.len() {
                        let child_entry = entry.entries.get(i).unwrap();
                        if i == entry.entries.len() - 1 {
                            println!("0x{:0>8x} }} ", child_entry.value);
                        } else {
                            print!("0x{:0>8x}, ", child_entry.value);
                        }
                    }
                } else {
                    println!("int {} 0x{:0>8x}", res_type, entry.value);
                }
                found = true;
                break;
            }
        }
        if !found {
            return ExitCode::from(6);
        }
    }

    ExitCode::SUCCESS
}
