use std::{
    fs::{create_dir_all, File},
    io::Write,
    io::{self, BufWriter},
    path::{Path, PathBuf},
};

use crate::{
    resource::{ResourceEntry, ResourceType},
    R55_VERSION,
};

use super::R55Writer;

/// Generates a valid R.java code which is written into the provided
/// writer. Only accepts buffered writer since this function produces
/// thousands of write syscalls on average.
pub fn write_java<W: Sized + Write>(
    writer: &mut BufWriter<W>,
    groups: Vec<(ResourceType, Vec<ResourceEntry>)>,
    package: &str,
) -> io::Result<()> {
    writeln!(writer, "/** Auto generated file by r55 v{}.\n *  DO NOT MODIFY.\n *  The next auto generation will overwrite\n *  changes made to this file\n**/", R55_VERSION)?;
    writeln!(writer, "package {package};\n")?;
    writeln!(writer, "public final class R {{")?;

    for (res_type, entries) in groups {
        writeln!(writer, "  public static final class {} {{", res_type)?;
        for entry in entries {
            if entry.entries.is_empty() && !entry.is_array {
                writeln!(
                    writer,
                    "    public static final int {}=0x{:0>8x};",
                    entry.id, entry.value
                )?;
            } else {
                let mut array_entries_buffer = String::new();
                write!(
                    writer,
                    "      public static final int[] {} = {{\n        ",
                    entry.id
                )?;

                for i in 0..entry.entries.len() {
                    let child_entry = entry.entries.get(i).unwrap();
                    if i == entry.entries.len() - 1 {
                        write!(writer, "0x{:0>8x}\n      ", child_entry.value)?;
                    } else if (i + 1) % 5 == 0 {
                        write!(writer, "0x{:0>8x},\n        ", child_entry.value)?;
                    } else {
                        write!(writer, "0x{:0>8x}, ", child_entry.value)?;
                    }

                    array_entries_buffer.push_str(
                        format!("      public static final int {}={};\n", child_entry.id, i)
                            .as_str(),
                    );
                }
                writeln!(writer, "}};")?;
                writeln!(writer, "{}", array_entries_buffer)?;
            }
        }
        writeln!(writer, "  }}")?;
    }
    writeln!(writer, "}}")?;
    Ok(())
}

pub struct JavaWriter {
    input_file: PathBuf,
    output_path: PathBuf,
    package: String,
}

impl JavaWriter {
    pub fn new(input_file: PathBuf, output_path: &Path, package: &str) -> JavaWriter {
        let mut out = output_path.to_path_buf();
        for p in package.split('.') {
            out.push(p);
        }
        JavaWriter {
            input_file,
            output_path: out,
            package: package.to_string(),
        }
    }
    pub fn get_output_writer(&self) -> Result<BufWriter<File>, io::Error> {
        create_dir_all(&self.output_path)?;
        let mut output_file = self.output_path.clone();
        output_file.push("R.java");
        let file = File::create(output_file)?;
        let buffer = BufWriter::new(file);
        Ok(buffer)
    }
}

impl R55Writer for JavaWriter {
    fn get_input_file(&self) -> &PathBuf {
        &self.input_file
    }
    fn write(&self, groups: Vec<(ResourceType, Vec<ResourceEntry>)>) -> std::io::Result<usize> {
        let mut writer = self.get_output_writer()?;
        write_java(&mut writer, groups, &self.package)?;
        Ok(0)
    }
}
