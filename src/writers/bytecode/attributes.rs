pub trait Attribute {
    /// The bytes to write that describe this attribute
    fn get_bytes(&self) -> Vec<u8>;
    /// where can i find the attribute name in the constants pool
    fn get_name_index(&self) -> u16;
}

//--------------------------------------------
/// ConstantValue attribute
pub struct ConstantValueAttribute {
    value: u16,
    name_index: u16,
}

impl ConstantValueAttribute {
    pub fn new(name_index: u16, value: u16) -> Self {
        Self {
            name_index, // where in the constants pool can i find the 'ConstantValue' string
            value,
        }
    }
}
impl Attribute for ConstantValueAttribute {
    fn get_name_index(&self) -> u16 {
        self.name_index
    }
    fn get_bytes(&self) -> Vec<u8> {
        Vec::from(self.value.to_be_bytes())
    }
}

//--------------------------------------------
/// Code attribute
pub struct CodeAttribute {
    name_index: u16,
    pub max_stack: u16,
    pub max_locals: u16,
    code: Vec<u8>,
    // we ain't going to take care of exceptions that much since we don't need them.
    exceptions: Vec<u8>,
    attributes: Vec<u8>,
}

impl CodeAttribute {
    pub fn new(name_index: u16) -> Self {
        Self {
            name_index,
            exceptions: Vec::new(),
            attributes: Vec::new(),
            max_stack: 1,
            max_locals: 1,
            code: Vec::new(),
        }
    }
    pub fn set_code(&mut self, code: Vec<u8>) {
        self.code = code;
    }
}

impl Attribute for CodeAttribute {
    fn get_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.extend(self.max_stack.to_be_bytes().iter());
        bytes.extend(self.max_locals.to_be_bytes().iter());

        let code_length: u32 = self.code.len() as u32;
        bytes.extend(code_length.to_be_bytes().iter());
        bytes.extend(self.code.iter());

        let exception_length: u16 = self.exceptions.len() as u16;
        bytes.extend(exception_length.to_be_bytes().iter());
        bytes.extend(self.exceptions.iter());

        let attributes_count: u16 = self.attributes.len() as u16;
        bytes.extend(attributes_count.to_be_bytes());
        bytes.extend(self.attributes.iter());

        bytes
    }
    fn get_name_index(&self) -> u16 {
        self.name_index
    }
}

//--------------------------------------------
/// NestHost attribute
pub struct NestHost {
    attr_name_index: u16,
    class_index: u16,
}

impl NestHost {
    pub fn new(attr_name_index: u16, class_index: u16) -> Self {
        Self {
            attr_name_index,
            class_index,
        }
    }
}

impl Attribute for NestHost {
    fn get_name_index(&self) -> u16 {
        self.attr_name_index
    }
    fn get_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.extend(self.class_index.to_be_bytes().iter());
        bytes
    }
}
//--------------------------------------------
/// InnerClasses attribute
pub struct InnerClasses {
    attr_name_index: u16,
    classes: Vec<u8>,
    classes_count: u16,
}

impl InnerClasses {
    pub fn new(attr_name_index: u16) -> Self {
        Self {
            attr_name_index,
            classes: Vec::new(),
            classes_count: 0,
        }
    }

    pub fn add_class(
        &mut self,
        inner_class_info_index: u16,
        outer_class_info_index: u16,
        inner_name_index: u16,
        inner_class_access_flags: u16,
    ) {
        // inner_class_info_index: u16,
        // outer_class_info_index: u16,
        // inner_name_index: u16
        // inner_class_access_flags: u16
        self.classes
            .extend(inner_class_info_index.to_be_bytes().iter());
        self.classes
            .extend(outer_class_info_index.to_be_bytes().iter());
        self.classes.extend(inner_name_index.to_be_bytes().iter());
        self.classes
            .extend(inner_class_access_flags.to_be_bytes().iter());

        self.classes_count += 1;
    }
}

impl Attribute for InnerClasses {
    fn get_name_index(&self) -> u16 {
        self.attr_name_index
    }

    fn get_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.extend(self.classes_count.to_be_bytes().iter());
        bytes.extend(self.classes.iter());
        bytes
    }
}

//--------------------------------------------
// NestMember attribute
pub struct NestMembers {
    attribute_name_index: u16,
    classes: Vec<u16>,
}

impl NestMembers {
    pub fn new(attribute_name_index: u16) -> Self {
        Self {
            attribute_name_index,
            classes: Vec::new(),
        }
    }

    pub fn add_class(&mut self, class_index: u16) {
        self.classes.push(class_index);
    }
}

impl Attribute for NestMembers {
    fn get_bytes(&self) -> Vec<u8> {
        // NestMembers_attribute
        // attribute_name_index: u16
        // attribute_length: u32
        // number_of_classes: u2
        // classes[u2; number_of_classes];
        // }
        let mut bytes = Vec::new();
        let count = self.classes.len() as u16;
        bytes.extend(count.to_be_bytes().iter());
        for class in &self.classes {
            bytes.extend(class.to_be_bytes().iter());
        }

        bytes
    }
    fn get_name_index(&self) -> u16 {
        self.attribute_name_index
    }
}
