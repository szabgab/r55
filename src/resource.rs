use std::fmt::Display;

use clap::{builder::PossibleValue, ValueEnum};

/// Android supported resource types
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum ResourceType {
    // anim
    Anim,
    // animator
    Animator,
    // array
    Array,
    // attr
    Attr,
    // ^attr-private
    AttrPrivate,
    // bool
    Bool,
    // color
    Color,
    // configVarying
    ConfigVarying,
    // dimen
    Dimen,
    // drawable
    Drawable,
    // font
    Font,
    // fraction
    Fraction,
    // id
    Id,
    // integer
    Integer,
    // interpolator
    Interpolator,
    // layout
    Layout,
    // macro
    Macro,
    // menu
    Menu,
    // mipmap
    Mipmap,
    // navigation
    Navigation,
    // plurals
    Plurals,
    // raw
    Raw,
    // string
    String,
    // style
    Style,
    // styleable
    Styleable,
    // transition
    Transition,
    // xml
    Xml,
    // unknown_res  -- The resource cannot be parsed
    UnknownRes,
}

impl From<&str> for ResourceType {
    /// Converts the string provided into its equivalent ResourceType enum entry
    fn from(value: &str) -> Self {
        match value {
            "anim" => ResourceType::Anim,
            "animator" => ResourceType::Animator,
            "array" => ResourceType::Array,
            "attr" => ResourceType::Attr,
            "^attr-private" => ResourceType::AttrPrivate,
            "bool" => ResourceType::Bool,
            "color" => ResourceType::Color,
            "configVarying" => ResourceType::ConfigVarying,
            "dimen" => ResourceType::Dimen,
            "drawable" => ResourceType::Drawable,
            "font" => ResourceType::Font,
            "fraction" => ResourceType::Fraction,
            "id" => ResourceType::Id,
            "integer" => ResourceType::Integer,
            "interpolator" => ResourceType::Interpolator,
            "layout" => ResourceType::Layout,
            "macro" => ResourceType::Macro,
            "menu" => ResourceType::Menu,
            "mipmap" => ResourceType::Mipmap,
            "navigation" => ResourceType::Navigation,
            "plurals" => ResourceType::Plurals,
            "raw" => ResourceType::Raw,
            "string" => ResourceType::String,
            "style" => ResourceType::Style,
            "styleable" => ResourceType::Styleable,
            "transition" => ResourceType::Transition,
            "xml" => ResourceType::Xml,
            _ => ResourceType::UnknownRes,
        }
    }
}

impl From<ResourceType> for String {
    fn from(val: ResourceType) -> Self {
        match val {
            ResourceType::Anim => String::from("anim"),
            ResourceType::Animator => String::from("animator"),
            ResourceType::Array => String::from("array"),
            ResourceType::Attr => String::from("attr"),
            ResourceType::AttrPrivate => String::from("^attr-private"),
            ResourceType::Bool => String::from("bool"),
            ResourceType::Color => String::from("color"),
            ResourceType::ConfigVarying => String::from("configVarying"),
            ResourceType::Dimen => String::from("dimen"),
            ResourceType::Drawable => String::from("drawable"),
            ResourceType::Font => String::from("font"),
            ResourceType::Fraction => String::from("fraction"),
            ResourceType::Id => String::from("id"),
            ResourceType::Integer => String::from("integer"),
            ResourceType::Interpolator => String::from("interpolator"),
            ResourceType::Layout => String::from("layout"),
            ResourceType::Macro => String::from("macro"),
            ResourceType::Menu => String::from("menu"),
            ResourceType::Mipmap => String::from("mipmap"),
            ResourceType::Navigation => String::from("navigation"),
            ResourceType::Plurals => String::from("plurals"),
            ResourceType::Raw => String::from("raw"),
            ResourceType::String => String::from("string"),
            ResourceType::Style => String::from("style"),
            ResourceType::Styleable => String::from("styleable"),
            ResourceType::Transition => String::from("transition"),
            ResourceType::Xml => String::from("xml"),
            ResourceType::UnknownRes => String::new(),
        }
    }
}
impl Display for ResourceType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str: String = ResourceType::into(*self);
        write!(f, "{}", str)
    }
}

impl From<&ResourceType> for usize {
    fn from(value: &ResourceType) -> Self {
        match value {
            ResourceType::Anim => 0,
            ResourceType::Animator => 1,
            ResourceType::Array => 2,
            ResourceType::Attr => 3,
            ResourceType::AttrPrivate => 4,
            ResourceType::Bool => 5,
            ResourceType::Color => 6,
            ResourceType::ConfigVarying => 7,
            ResourceType::Dimen => 8,
            ResourceType::Drawable => 9,
            ResourceType::Font => 10,
            ResourceType::Fraction => 11,
            ResourceType::Id => 12,
            ResourceType::Integer => 13,
            ResourceType::Interpolator => 14,
            ResourceType::Layout => 15,
            ResourceType::Macro => 16,
            ResourceType::Menu => 17,
            ResourceType::Mipmap => 18,
            ResourceType::Navigation => 19,
            ResourceType::Plurals => 20,
            ResourceType::Raw => 21,
            ResourceType::String => 22,
            ResourceType::Style => 23,
            ResourceType::Styleable => 24,
            ResourceType::Transition => 25,
            ResourceType::Xml => 26,
            ResourceType::UnknownRes => 27,
        }
    }
}

impl ValueEnum for ResourceType {
    fn value_variants<'a>() -> &'a [Self] {
        &[
            ResourceType::Anim,
            ResourceType::Animator,
            ResourceType::Array,
            ResourceType::Attr,
            ResourceType::AttrPrivate,
            ResourceType::Bool,
            ResourceType::Color,
            ResourceType::ConfigVarying,
            ResourceType::Dimen,
            ResourceType::Drawable,
            ResourceType::Font,
            ResourceType::Fraction,
            ResourceType::Id,
            ResourceType::Integer,
            ResourceType::Interpolator,
            ResourceType::Layout,
            ResourceType::Macro,
            ResourceType::Menu,
            ResourceType::Mipmap,
            ResourceType::Navigation,
            ResourceType::Plurals,
            ResourceType::Raw,
            ResourceType::String,
            ResourceType::Style,
            ResourceType::Styleable,
            ResourceType::Transition,
            ResourceType::Xml,
        ]
    }
    fn from_str(input: &str, _ignore_case: bool) -> Result<Self, String> {
        let res_type = ResourceType::from(input);
        if matches!(res_type, ResourceType::UnknownRes) {
            return Err(format!("Unknown resource type {input}"));
        }
        Ok(res_type)
    }

    fn to_possible_value(&self) -> Option<clap::builder::PossibleValue> {
        if matches!(self, ResourceType::UnknownRes) {
            return None;
        }
        Some(PossibleValue::new(self.to_string()))
    }
}
#[derive(Debug, Clone)]
pub struct ResourceEntry {
    pub id: String,
    pub res_type: ResourceType,
    pub value: u32,
    pub is_array: bool,
    pub entries: Vec<ResourceArrayEntry>,
}

#[derive(Debug, Clone)]
pub struct ResourceArrayEntry {
    pub id: String,
    pub value: u32,
}

impl ResourceEntry {
    pub fn new() -> Self {
        ResourceEntry {
            id: String::new(),
            res_type: ResourceType::UnknownRes,
            value: 0,
            entries: Vec::new(),
            is_array: false,
        }
    }
}

impl Default for ResourceEntry {
    fn default() -> Self {
        Self::new()
    }
}
