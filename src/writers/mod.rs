use std::path::PathBuf;

use crate::resource::{ResourceEntry, ResourceType};

pub mod bytecode;
pub mod java;

pub trait R55Writer {
    /// returns the input R.txt file
    fn get_input_file(&self) -> &PathBuf;
    fn write(&self, groups: Vec<(ResourceType, Vec<ResourceEntry>)>) -> std::io::Result<usize>;
}
