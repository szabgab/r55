mod attributes;

use std::{
    fs::{create_dir_all, File},
    io::{BufWriter, Write},
    path::{Path, PathBuf},
};

use crate::resource::{ResourceEntry, ResourceType};

use self::attributes::{
    Attribute, CodeAttribute, ConstantValueAttribute, InnerClasses, NestHost, NestMembers,
};

use super::R55Writer;

const MAGIC_BYTES: u32 = 0xcafebabe;
const VERSION_MAJOR: u16 = 61;
const VERSION_MINOR: u16 = 0;

mod class_access_flags {
    pub const PUBLIC: u16 = 0x0001;
    pub const FINAL: u16 = 0x0010;
    pub const SUPER: u16 = 0x0020;
    // pub const INTERFACE: u16 = 0x0200;
    // pub const ABSTRACT: u16 = 0x0400;
    // pub const SYNTHETIC: u16 = 0x1000;
    // pub const ANNOTATION: u16 = 0x2000;
    // pub const ENUM: u16 = 0x4000;
}
// yes, the specification has different enums every time for same flags
mod inner_class_access_flags {
    pub const PUBLIC: u16 = 0x0001;
    pub const STATIC: u16 = 0x0008;
    pub const FINAL: u16 = 0x0010;
}

mod field_access_flags {
    pub const PUBLIC: u16 = 0x0001;
    // pub const PRIVATE: u16 = 0x0002;
    // pub const PROTECTED: u16 = 0x0004;
    pub const STATIC: u16 = 0x0008;
    pub const FINAL: u16 = 0x0010;
    // pub const VOLATILE: u16 = 0x0040;
    // pub const TRANSIENT: u16 = 0x0080;
    // pub const SYNTHETIC: u16 = 0x1000;
    // pub const ENUM: u16 = 0x4000;
}

/// All the bytecode instructions that r55 is going to use,
/// Their documentation format is given as
/// description
/// arguments - subsequent bytes that follow the instruction
/// [`stack before`] -> [`stack after operation`]
/// more info at https://en.m.wikipedia.org/wiki/List_of_Java_bytecode_instructions
mod opcodes {
    /// load a reference onto the stack from local variable 0
    /// -> objectref
    pub const ALOAD_0: u8 = 0x2a;
    /// invoke instance method on object objectref and puts the result on the stack (might be void); the method is identified
    /// by method reference index in constant pool (indexbyte1 << 8 | indexbyte2)
    /// 2 args: indexbyte1, indexbyte2
    /// objectref, [arg1, arg2, ...] -> result
    pub const INVOKESPECIAL: u8 = 0xb7;
    /// return void from method
    /// -> [empty]
    pub const RETURN: u8 = 0xb1;
    /// push a byte onto the stack as an integer value
    /// 1 arg: value
    /// -> value
    pub const BIPUSH: u8 = 0x10;
    /// push a short onto the stack as an integer value
    /// 2 args: byte1, byte2
    /// -> value
    pub const SIPUSH: u8 = 0x11;
    /// duplicate the value on top of the stack
    /// value -> value, value
    pub const DUP: u8 = 0x59;
    /// store an int into an array
    /// arrayref, index, value ->
    pub const IASTORE: u8 = 0x4f;
    /// load the int value 0 onto the stack
    /// -> 0
    pub const ICONST_0: u8 = 0x03;
    /// load the int value 1 onto the stack
    /// -> 1
    pub const ICONST_1: u8 = 0x04;
    /// load the int value 2 onto the stack
    /// -> 2
    pub const ICONST_2: u8 = 0x05;
    /// load the int value 3 onto the stack
    /// -> 3
    pub const ICONST_3: u8 = 0x06;
    /// load the int value 4 onto the stack
    /// -> 4
    pub const ICONST_4: u8 = 0x07;
    /// load the int value 5 onto the stack
    /// -> 5
    pub const ICONST_5: u8 = 0x08;
    /// push a constant #index from a constant pool (String, int, float, Class, java.lang.invoke.MethodType,
    /// java.lang.invoke.MethodHandle, or a dynamically-computed constant) onto the stack
    /// 1 arg: index
    /// -> value
    pub const LDC: u8 = 0x12;
    /// push a constant #index from a constant pool (String, int, float, Class,
    /// java.lang.invoke.MethodType, java.lang.invoke.MethodHandle, or a dynamically-computed constant) onto the stack (wide index is constructed as indexbyte1 << 8 | indexbyte2)
    /// 2 args: indexbyte1, indexbyte2
    /// -> value
    pub const LDC_W: u8 = 0x13;
    /// create new array with count elements of primitive type identified by atype
    /// 1 arg: count
    /// count -> arrayref
    pub const NEWARRAY: u8 = 0xbc;
    /// set static field to value in a class, where the field is identified by a field reference index in constant pool (indexbyte1 << 8 | indexbyte2)
    /// 2 args: indexbyte1, indexbyte2
    /// value ->
    pub const PUTSTATIC: u8 = 0xb3;
}

enum ConstantInfoTag {
    Utf8 = 1,
    Integer = 3,
    // Float = 4,
    // Long = 5,
    // Double = 6,
    Class = 7,
    // String = 8,
    Fieldref = 9,
    Methodref = 10,
    // InterfaceMethodref = 11,
    NameAndType = 12,
    // MethodHandle = 15,
    // MethodType = 16,
    // InvokeDynamic = 18,
    // Unknown = 69,
}

impl From<ConstantInfoTag> for u8 {
    fn from(value: ConstantInfoTag) -> Self {
        match value {
            ConstantInfoTag::Utf8 => 1,
            ConstantInfoTag::Integer => 3,
            // ConstantInfoTag::Long => 5,
            ConstantInfoTag::Class => 7,
            ConstantInfoTag::Fieldref => 9,
            ConstantInfoTag::Methodref => 10,
            ConstantInfoTag::NameAndType => 12,
            // ConstantInfoTag::MethodHandle => 15,
            // ConstantInfoTag::MethodType => 16,
            // ConstantInfoTag::Unknown => 69,
        }
    }
}

struct ConstantPoolWriter {
    constant_pool: Vec<Vec<u8>>,
}

impl ConstantPoolWriter {
    pub fn new() -> ConstantPoolWriter {
        ConstantPoolWriter {
            constant_pool: Vec::new(),
        }
    }
    /// Get the current constants pool index, the constants pool
    /// is indexed from 1
    pub fn get_index(&self) -> u16 {
        (self.constant_pool.len() + 1) as u16
    }
    /// Adds a java initializer method onto the constants pool
    /// Should be the first method called when starting constants pool entries
    /// Returns a tuple of created indexes
    /// (class_index, descriptor_index, name_index, methodref_index)
    pub fn create_class_init_method(&mut self) -> (u16, u16, u16, u16) {
        let name = "<init>";
        let class = "java/lang/Object";
        let descriptor = "()V";

        // for some reason This MethodRef is always at cp index 1
        // but I'm going to ignore this in an effort to avoid creating index bugs and unnecessary logic complexity
        // We don't need aesthetic. Unless this is somehow useful to JVM

        // CONSTANT_Methodref_info {
        //    tag: u8,
        //    class_index: u16,
        //    name_and_type_index: u16
        // }

        let class_index = self.create_class(class);
        let name_index = self.create_utf8(name);
        let descriptor_index = self.create_utf8(descriptor);
        let name_and_type_index = self.create_name_and_type(name_index, descriptor_index);
        let methodref_index = self.create_methodref(class_index, name_and_type_index);

        // this is reverse of what JVM expects.

        // return everything we may need
        (class_index, descriptor_index, name_index, methodref_index)
    }

    /// creates a class info entry and returns its constant pool index(indexed from 1).
    /// it is also assumed that class is a utf8 string not previously inserted onto the constant pool table
    pub fn create_class(&mut self, class: &str) -> u16 {
        let index = self.get_index();
        let name_index = index + 1;

        let mut class_info: Vec<u8> = Vec::new();
        class_info.push(ConstantInfoTag::Class.into());
        class_info.extend(name_index.to_be_bytes().iter());
        self.constant_pool.push(class_info);

        self.create_utf8(class);

        index
    }

    /// creates a utf8 string an returns its index on the constants pool table(indexed from 1).
    /// it is also assumes that the string is a utf8 string not previously inserted onto the constant pool table
    pub fn create_utf8(&mut self, string: &str) -> u16 {
        let index = self.get_index();
        let length = string.len() as u16;

        let mut utf8_info: Vec<u8> = Vec::new();
        utf8_info.push(ConstantInfoTag::Utf8.into());
        utf8_info.extend(length.to_be_bytes().iter());
        utf8_info.extend(string.as_bytes().iter());
        self.constant_pool.push(utf8_info);

        index
    }

    pub fn create_methodref(&mut self, class_index: u16, name_and_type_index: u16) -> u16 {
        let index = self.get_index();
        let mut method_ref: Vec<u8> = Vec::new();
        method_ref.push(ConstantInfoTag::Methodref.into());
        method_ref.extend(class_index.to_be_bytes().iter());
        method_ref.extend(name_and_type_index.to_be_bytes().iter());
        self.constant_pool.push(method_ref);

        index
    }

    pub fn create_name_and_type(&mut self, name_index: u16, descriptor_index: u16) -> u16 {
        let index = self.get_index();
        let mut name_type: Vec<u8> = Vec::new();
        name_type.push(ConstantInfoTag::NameAndType.into());
        name_type.extend(name_index.to_be_bytes().iter());
        name_type.extend(descriptor_index.to_be_bytes().iter());
        self.constant_pool.push(name_type);

        index
    }

    /// creates an integer entry and returns the address on the constants pool table(indexed from 1).
    /// Assumes that this is a valid integer and not previously inserted into constants pool table
    pub fn create_integer(&mut self, integer: i32) -> u16 {
        let index = self.get_index();

        let mut integer_info: Vec<u8> = Vec::new();
        integer_info.push(ConstantInfoTag::Integer.into());
        integer_info.extend(integer.to_be_bytes().iter());
        self.constant_pool.push(integer_info);

        index
    }

    pub fn create_fieldref(&mut self, class_index: u16, name_and_type_index: u16) -> u16 {
        // CONSTANT_Fieldref_info {
        //    tag: u8,
        //    class_index: u16,
        //    name_and_type_index: u16
        // }
        let index = self.get_index();
        let mut fieldref: Vec<u8> = Vec::new();
        fieldref.push(ConstantInfoTag::Fieldref.into());
        fieldref.extend(class_index.to_be_bytes().iter());
        fieldref.extend(name_and_type_index.to_be_bytes().iter());
        self.constant_pool.push(fieldref);

        index
    }

    pub fn write(&mut self, writer: &mut BufWriter<dyn Write>) -> std::io::Result<usize> {
        let constant_pool_count: u16 = (self.constant_pool.len() + 1) as u16;
        let mut written = 0;
        written += writer.write(&constant_pool_count.to_be_bytes())?;

        for constant_info in &self.constant_pool {
            written += writer.write(constant_info)?;
        }
        Ok(written)
    }
}

struct FieldInfoWriter {
    fields: Vec<u8>,
    fields_count: u16,
}

impl FieldInfoWriter {
    pub fn new() -> FieldInfoWriter {
        FieldInfoWriter {
            fields: Vec::new(),
            fields_count: 0,
        }
    }

    pub fn create_field(
        &mut self,
        access_flags: u16,
        name_index: u16,
        descriptor_index: u16,
        attributes: Vec<&dyn Attribute>,
    ) {
        let attributes_count = attributes.len() as u16;
        // field info table
        // access_flags: u16
        // name_index: u16
        // descriptor_index: u16
        // attributes_count: u16
        // attributes: [attributes_count]

        // access_flags
        self.fields.extend(access_flags.to_be_bytes().iter());
        self.fields.extend(name_index.to_be_bytes().iter());
        self.fields.extend(descriptor_index.to_be_bytes().iter());
        self.fields.extend(attributes_count.to_be_bytes().iter());

        // The attributes
        for attr in attributes {
            // attribute table
            // attribute_name_index : u16
            // attribute_length: u32
            // info: [u8; attribute_length]
            let attr_name_index = attr.get_name_index();
            let bytes = attr.get_bytes();
            let attr_length = bytes.len() as u32;
            self.fields.extend(attr_name_index.to_be_bytes().iter());
            self.fields.extend(attr_length.to_be_bytes().iter());
            self.fields.extend(bytes.iter());
        }
        self.fields_count += 1;
    }
    pub fn write<W: Write + ?Sized>(&self, writer: &mut BufWriter<W>) -> std::io::Result<usize> {
        let count = self.fields_count;
        let count_written = writer.write(&count.to_be_bytes())?;
        let total_fields_written = writer.write(&self.fields)?;
        Ok(count_written + total_fields_written)
    }
}

struct MethodInfoWriter {
    methods: Vec<u8>,
    methods_count: u16,
}
impl MethodInfoWriter {
    pub fn new() -> Self {
        Self {
            methods: Vec::new(),
            methods_count: 0,
        }
    }

    pub fn create_method(
        &mut self,
        access_flags: u16,
        name_index: u16,
        descriptor_index: u16,
        attributes: Vec<&dyn Attribute>,
    ) {
        // create class init method
        // method_info {
        //    access_flags: u16,
        //    name_index: u16,
        //    descriptor_index: u16
        //    attributes_count: u16
        //    attribute_info attributes[attributes_count]
        // }
        self.methods.extend(access_flags.to_be_bytes().iter());
        self.methods.extend(name_index.to_be_bytes().iter());
        self.methods.extend(descriptor_index.to_be_bytes().iter());

        let attributes_count = attributes.len() as u16;
        self.methods.extend(attributes_count.to_be_bytes().iter());
        // The attributes
        for attr in attributes {
            // attribute table
            // attribute_name_index : u16
            // attribute_length: u32
            // info: [u8; attribute_length]
            let attr_name_index = attr.get_name_index();
            let bytes = attr.get_bytes();
            let attr_length = bytes.len() as u32;
            self.methods.extend(attr_name_index.to_be_bytes().iter());
            self.methods.extend(attr_length.to_be_bytes().iter());
            self.methods.extend(bytes.iter());
        }
        self.methods_count += 1;
    }

    pub fn write<W: ?Sized + Write>(&self, writer: &mut BufWriter<W>) -> std::io::Result<usize> {
        let count = self.methods_count;
        let count_written = writer.write(&count.to_be_bytes())?;
        let total_method_bytes_written = writer.write(&self.methods)?;
        Ok(count_written + total_method_bytes_written)
    }
}

struct AttributesTableWriter {
    attributes: Vec<u8>,
    attributes_count: u16,
}

impl AttributesTableWriter {
    pub fn new() -> Self {
        Self {
            attributes: Vec::new(),
            attributes_count: 0,
        }
    }

    pub fn add_attribute(&mut self, attr: &dyn Attribute) {
        // attribute_info {
        //    attribute_name_index: u16,
        //    attribute_length: u32,
        //    bytes: [u8; attribute_length];
        // }

        let name_index = attr.get_name_index();
        let bytes = attr.get_bytes();
        let length = bytes.len() as u32;

        self.attributes.extend(name_index.to_be_bytes().iter());
        self.attributes.extend(length.to_be_bytes().iter());
        self.attributes.extend(bytes.iter());

        self.attributes_count += 1;
    }

    pub fn write<W: ?Sized + Write>(&self, writer: &mut BufWriter<W>) -> std::io::Result<usize> {
        // attributes count: u16
        // attributes [count]
        let mut total_bytes = 0;
        total_bytes += writer.write(&self.attributes_count.to_be_bytes())?;
        total_bytes += writer.write(&self.attributes)?;

        Ok(total_bytes)
    }
}

pub struct ClassWriter<'a> {
    writer: &'a mut BufWriter<dyn Write>,
    package: String,
    constants_pool: ConstantPoolWriter,
    fields_pool: FieldInfoWriter,
    methods_pool: MethodInfoWriter,
    attributes_pool: AttributesTableWriter,

    resource_type: ResourceType,
    entries: &'a Vec<ResourceEntry>,
}

impl<'a> ClassWriter<'a> {
    /// Creates a new Resource class writer
    pub fn new(
        writer: &'a mut BufWriter<dyn Write>,
        package: &str,
        resource_type: ResourceType,
        entries: &'a Vec<ResourceEntry>,
    ) -> Self {
        let package = package.replace('.', "/");
        Self {
            writer,
            package,
            resource_type,
            entries,
            constants_pool: ConstantPoolWriter::new(),
            fields_pool: FieldInfoWriter::new(),
            methods_pool: MethodInfoWriter::new(),
            attributes_pool: AttributesTableWriter::new(),
        }
    }

    fn write_header(&mut self) -> std::io::Result<usize> {
        let mut written = 0;
        written += self.writer.write(&MAGIC_BYTES.to_be_bytes())?;
        written += self.writer.write(&VERSION_MINOR.to_be_bytes())?;
        written += self.writer.write(&VERSION_MAJOR.to_be_bytes())?;

        Ok(written)
    }

    /// Writes the java class init method
    fn write_init_method(
        &mut self,
        methodref_index: u16,
        name_index: u16,
        descriptor_index: u16,
        code_attr_name_index: u16,
    ) {
        // ===== bytecode =====
        // aload_0
        // invokespecial @methodref_index
        // return
        // ====================
        let mut code = vec![opcodes::ALOAD_0, opcodes::INVOKESPECIAL];

        // add methodref index
        code.extend(methodref_index.to_be_bytes().iter());
        code.push(opcodes::RETURN);

        let access_flags: u16 = 0x0001; // public
        let name_index: u16 = name_index; // <init>
        let descriptor_index: u16 = descriptor_index; // ()v
        let mut code_attibute = CodeAttribute::new(code_attr_name_index);
        code_attibute.set_code(code);
        self.methods_pool.create_method(
            access_flags,
            name_index,
            descriptor_index,
            vec![&code_attibute],
        );
    }

    /// selects what value to push onto the stack based on the size provided,
    /// conveniently only works with unsigned integers since its highly unlikely
    /// The use case of r55 to involve signed numbers
    fn bytecode_push_value(&mut self, code: &mut Vec<u8>, value: u16) {
        match value {
            0 => code.push(opcodes::ICONST_0),
            1 => code.push(opcodes::ICONST_1),
            2 => code.push(opcodes::ICONST_2),
            3 => code.push(opcodes::ICONST_3),
            4 => code.push(opcodes::ICONST_4),
            5 => code.push(opcodes::ICONST_5),
            6..=255 => {
                code.push(opcodes::BIPUSH);
                code.push(value as u8);
            }
            _ => {
                code.push(opcodes::SIPUSH);
                code.extend(value.to_be_bytes().iter());
            }
        }
    }

    /// Loads a value from the constant pool, switches the opcode based on the argument
    /// size, can have an indexable max of u16::MAX.
    fn bytecode_load_c(&mut self, code: &mut Vec<u8>, value: u16) {
        match value {
            0..=255 => {
                code.push(opcodes::LDC);
                code.push(value as u8);
            }
            _ => {
                code.push(opcodes::LDC_W);
                code.extend(value.to_be_bytes().iter());
            }
        }
    }

    /// Creates a static initializer method for array entries, only call this if you have
    /// array type entries
    fn write_static_init_method(
        &mut self,
        bytecode: Vec<u8>,
        name_index: u16,
        void_descriptor_index: u16,
        code_attr_name_index: u16,
    ) {
        let access_flags = 0x0008; // static
        let mut code_attr = CodeAttribute::new(code_attr_name_index);
        code_attr.set_code(bytecode);
        code_attr.max_locals = 0;
        code_attr.max_stack = 4; // The stack never grows past 4
        self.methods_pool.create_method(
            access_flags,
            name_index,
            void_descriptor_index,
            vec![&code_attr],
        );
    }

    /// writes java bytecode to initialize java array and load it onto the static section.
    fn create_static_array(
        &mut self,
        code: &mut Vec<u8>,
        start_index: u16,
        length: u16,
        field_ref: u16,
    ) {
        const ARRAY_TYPE: u8 = 10; // T_INT

        // create new array of size length
        self.bytecode_push_value(code, length);
        // Load from constant pool

        code.push(opcodes::NEWARRAY); // new array
        code.push(ARRAY_TYPE);

        for index in 0..length {
            let int_index = index + start_index;
            code.push(opcodes::DUP);
            self.bytecode_push_value(code, index);
            self.bytecode_load_c(code, int_index);
            code.push(opcodes::IASTORE);
        }

        code.push(opcodes::PUTSTATIC);
        code.extend(field_ref.to_be_bytes().iter());
    }
    // tries to write a Resource inner class i.e. things like anim, styleable etc.
    pub fn write_inner_class(&mut self) -> std::io::Result<usize> {
        let mut written = 0;
        // MAGIC BYTES and VERSION
        written += self.write_header()?;

        // write the class entry point
        let (super_class_index, init_descriptor_index, init_name_index, init_methodref_index) =
            self.constants_pool.create_class_init_method();

        let class = format!("{}/R${}", self.package, self.resource_type);
        let class_host = format!("{}/R", self.package);
        let this_class_index = self.constants_pool.create_class(&class);

        // constant value attribute name
        let constant_value_attr_index = self.constants_pool.create_utf8("ConstantValue");
        let integer_descriptor = self.constants_pool.create_utf8("I");
        let mut integer_array_descriptor: Option<u16> = None;

        let access_flags =
            field_access_flags::PUBLIC | field_access_flags::FINAL | field_access_flags::STATIC;

        let mut static_init_bytecode = Vec::new();

        for entry in self.entries {
            if !entry.is_array {
                let constant_value = self.constants_pool.create_integer(entry.value as i32);
                let name_index = self.constants_pool.create_utf8(entry.id.as_str());
                let constant_attribute =
                    ConstantValueAttribute::new(constant_value_attr_index, constant_value);
                self.fields_pool.create_field(
                    access_flags,
                    name_index,
                    integer_descriptor,
                    vec![&constant_attribute],
                );
            } else {
                // entry is an array
                let descriptor = if let Some(descriptor) = integer_array_descriptor {
                    descriptor
                } else {
                    let i = self.constants_pool.create_utf8("[I");
                    integer_array_descriptor = Some(i);
                    i
                };
                // for easy writing of static initializer code we have to group everything together
                // store the current constants pool index, this is the start index of our array entries
                let index = self.constants_pool.get_index();
                for child_entry in &entry.entries {
                    self.constants_pool.create_integer(child_entry.value as i32);
                }
                // write field ref
                let name_index = self.constants_pool.create_utf8(&entry.id);
                let name_and_type_index = self
                    .constants_pool
                    .create_name_and_type(name_index, descriptor);
                let fieldref_index = self
                    .constants_pool
                    .create_fieldref(this_class_index, name_and_type_index);

                self.fields_pool
                    .create_field(access_flags, name_index, descriptor, vec![]);

                // write individual indexes
                for (i, child_entry) in entry.entries.iter().enumerate() {
                    let constant_value = self.constants_pool.create_integer(i as i32);
                    let name_index = self.constants_pool.create_utf8(&child_entry.id);
                    let constant_attribute =
                        ConstantValueAttribute::new(constant_value_attr_index, constant_value);
                    self.fields_pool.create_field(
                        access_flags,
                        name_index,
                        integer_descriptor,
                        vec![&constant_attribute],
                    );
                }
                self.create_static_array(
                    &mut static_init_bytecode,
                    index,
                    entry.entries.len() as u16,
                    fieldref_index,
                );
            }
        }
        let code_attr_name_index = self.constants_pool.create_utf8("Code");
        // Write our class constructor
        self.write_init_method(
            init_methodref_index,
            init_name_index,
            init_descriptor_index,
            code_attr_name_index,
        );

        if !static_init_bytecode.is_empty() {
            // don't forget to return
            static_init_bytecode.push(opcodes::RETURN);
            // has static array initializations
            let cint_name_index = self.constants_pool.create_utf8("<clinit>");

            self.write_static_init_method(
                static_init_bytecode,
                cint_name_index,
                init_descriptor_index,
                code_attr_name_index,
            );
        }

        // Write nest host i.e. the outer class where this belongs
        let nest_host_attr_index = self.constants_pool.create_utf8("NestHost");
        let nest_class_index = self.constants_pool.create_class(&class_host);
        self.attributes_pool
            .add_attribute(&NestHost::new(nest_host_attr_index, nest_class_index));

        // write inner classes attr
        let inner_classes_attr_name_index = self.constants_pool.create_utf8("InnerClasses");
        let inner_name_index = self
            .constants_pool
            .create_utf8(&self.resource_type.to_string());
        let mut inner_classes = InnerClasses::new(inner_classes_attr_name_index);
        inner_classes.add_class(
            this_class_index,
            nest_class_index,
            inner_name_index,
            inner_class_access_flags::PUBLIC
                | inner_class_access_flags::STATIC
                | inner_class_access_flags::FINAL,
        );
        self.attributes_pool.add_attribute(&inner_classes);

        written += self.constants_pool.write(self.writer)?;

        // CLASS INFO
        // Access flags
        let access_flags =
            class_access_flags::PUBLIC | class_access_flags::FINAL | class_access_flags::SUPER;

        written += self.writer.write(&access_flags.to_be_bytes())?;
        written += self.writer.write(&this_class_index.to_be_bytes())?;
        written += self.writer.write(&super_class_index.to_be_bytes())?;

        // INTERFACES
        // we don't need them
        let interfaces_count: u16 = 0x0000;
        written += self.writer.write(&interfaces_count.to_be_bytes())?;

        // write field
        written += self.fields_pool.write(self.writer)?;
        // write methods
        written += self.methods_pool.write(self.writer)?;
        // write attributes
        written += self.attributes_pool.write(self.writer)?;
        Ok(written)
    }
    pub fn write_outer_class(&mut self, members: &Vec<ResourceType>) -> std::io::Result<usize> {
        let mut written = 0;
        // MAGIC BYTES and VERSION
        written += self.write_header()?;

        // write the class entry point
        let (super_class_index, init_descriptor_index, init_name_index, init_methodref_index) =
            self.constants_pool.create_class_init_method();

        let class = format!("{}/R", self.package);
        let this_class_index = self.constants_pool.create_class(&class);

        let code_attr_name_index = self.constants_pool.create_utf8("Code");
        // Write our class constructor
        self.write_init_method(
            init_methodref_index,
            init_name_index,
            init_descriptor_index,
            code_attr_name_index,
        );

        // Attributes
        let nest_members_name_index = self.constants_pool.create_utf8("NestMembers");
        let inner_clases_attr_name_index = self.constants_pool.create_utf8("InnerClasses");

        let mut nest_members = NestMembers::new(nest_members_name_index);
        let mut inner_classes = InnerClasses::new(inner_clases_attr_name_index);

        for res_type in members {
            let class_name = format!("{}/R${}", self.package, res_type);
            let class_member_index = self.constants_pool.create_class(&class_name);
            nest_members.add_class(class_member_index);

            let inner_class_name_index = self
                .constants_pool
                .create_utf8(res_type.to_string().as_str());

            inner_classes.add_class(
                class_member_index,
                this_class_index,
                inner_class_name_index,
                inner_class_access_flags::PUBLIC
                    | inner_class_access_flags::STATIC
                    | inner_class_access_flags::FINAL,
            );
        }

        // save the attributes
        self.attributes_pool.add_attribute(&nest_members);
        self.attributes_pool.add_attribute(&inner_classes);

        // dump constants pool onto table
        written += self.constants_pool.write(self.writer)?;

        // CLASS INFO
        // Access flags
        let access_flags =
            class_access_flags::PUBLIC | class_access_flags::FINAL | class_access_flags::SUPER;

        written += self.writer.write(&access_flags.to_be_bytes())?;
        written += self.writer.write(&this_class_index.to_be_bytes())?;
        written += self.writer.write(&super_class_index.to_be_bytes())?;

        // INTERFACES
        // we don't need them
        let interfaces_count: u16 = 0x0000;
        written += self.writer.write(&interfaces_count.to_be_bytes())?;

        // write field
        written += self.fields_pool.write(self.writer)?;
        // write methods
        written += self.methods_pool.write(self.writer)?;
        // write attributes
        written += self.attributes_pool.write(self.writer)?;
        Ok(written)
    }
}

pub struct BytecodeWriter {
    input_file: PathBuf,
    output_path: PathBuf,

    package: String,
}

impl BytecodeWriter {
    pub fn new(input_file: PathBuf, output_path: &Path, package: &str) -> BytecodeWriter {
        let mut output_path = output_path.to_path_buf();
        for p in package.split('.') {
            output_path.push(p);
        }
        BytecodeWriter {
            input_file,
            output_path,
            package: package.to_string(),
        }
    }
}

impl R55Writer for BytecodeWriter {
    fn get_input_file(&self) -> &PathBuf {
        &self.input_file
    }
    fn write(&self, groups: Vec<(ResourceType, Vec<ResourceEntry>)>) -> std::io::Result<usize> {
        // write inner classes
        let mut written = 0;
        if !self.output_path.exists() {
            create_dir_all(&self.output_path)?;
        }

        for (res_type, entries) in &groups {
            let mut class_file = self.output_path.clone();
            class_file.push(format!("R${}.class", res_type));
            let file = File::create(&class_file)?;

            let mut writer = BufWriter::new(file);
            let mut class_writer = ClassWriter::new(&mut writer, &self.package, *res_type, entries);

            written += class_writer.write_inner_class()?;
        }
        // write outer class, R.class
        let mut class_file = self.output_path.clone();
        class_file.push("R.class");

        let file = File::create(&class_file)?;
        let mut writer = BufWriter::new(file);
        let entries_empty = Vec::new();
        let mut class_writer = ClassWriter::new(
            &mut writer,
            &self.package,
            crate::resource::ResourceType::UnknownRes,
            &entries_empty,
        );

        let members: Vec<ResourceType> = groups.iter().map(|e| e.0).collect();
        written += class_writer.write_outer_class(&members)?;
        Ok(written)
    }
}

// CafeBabe :-)
